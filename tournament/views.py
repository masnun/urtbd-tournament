from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from tournament.models import Registration, Team
from django.contrib.auth.decorators import login_required
from tournament.forms import RegistrationForm


def home(request):
    players = Registration.objects.all()
    if request.user.is_authenticated():
        nicknames = Registration.objects.all().filter(user=request.user)

        term_nick = "Register"
        if len(nicknames) > 0:
            term_nick = "Edit Nick"
    else:
        term_nick = ""

    data = {
        'players': players,
        'term_nick': term_nick
    }

    return render_to_response('tournament/home.html', data, context_instance=RequestContext(request))


@login_required
def register(request):
    registrations = Registration.objects.all().filter(user=request.user)
    if len(registrations) > 0:
        registration = registrations[0]
        form = RegistrationForm(instance=registration)
    else:
        form = RegistrationForm()

    if request.method == "POST":
        if len(registrations) > 0:
            registration = registrations[0]
        else:
            registration = Registration(user=request.user)

        form = RegistrationForm(request.POST, instance=registration)
        if form.is_valid():
            form.save()
            return redirect('/')

    return render_to_response('tournament/register.html', {'form': form}, context_instance=RequestContext(request))


def rulebook(request):
    return render_to_response('tournament/rulebook.html', context_instance=RequestContext(request))


def teams(request):
    teams = Team.objects.all()
    return render_to_response('tournament/teams.html', {'teams': teams}, context_instance=RequestContext(request))