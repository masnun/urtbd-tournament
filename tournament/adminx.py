import xadmin
from xadmin import views
from regform.models import Registration, Team


class MainDashboard(object):
    widgets = [
        [
            {"type": "html", "title": "Welcome!",
             "content": "<h3> Welcome to UrTBD Admin! </h3><p>What would you like to do today?</p>"},
            {"type": "list", "model": "regform.registration", 'params': {'o': '-id'}},
        ],
        [
            {
                "type": "qbutton",
                "title": "Quick Start",
                "btns": [
                    {'model': Registration},
                    {'model': Team},
                ]
            },
            {"type": "addform", "model": Registration},
            {"type": "addform", "model": Team},
        ]
    ]


xadmin.site.register(views.website.IndexView, MainDashboard)


class BaseSetting(object):
    enable_themes = True
    use_bootswatch = False


xadmin.site.register(views.BaseAdminView, BaseSetting)


class GolbeSetting(object):
    site_title = 'UrTBD Admin'
    globe_models_icon = {
        Registration: 'tasks',
        Team: 'tasks',

    }


xadmin.site.register(views.CommAdminView, GolbeSetting)


class RegistrationAdmin(object):
    list_display = ('nickname',)
    search_fields = ('nickname',)
    list_filter = ['nickname']


class TeamAdmin(object):
    list_display = ('name',)
    search_fields = ('name',)
    list_filter = ['name']


xadmin.site.register(Registration, RegistrationAdmin)
xadmin.site.register(Team, TeamAdmin)

