from django.forms import ModelForm, ValidationError
from regform.models import Registration


class RegistrationForm(ModelForm):
    def clean_nickname(self):
        nickname = self.cleaned_data['nickname']
        try:
            user = Registration.objects.get(nickname=nickname)
        except Registration.DoesNotExist:
            return nickname
        raise ValidationError(u'%s is already being used!' % nickname)

    class Meta:
        model = Registration
        exclude = ['user', 'team']