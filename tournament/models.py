from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Team(models.Model):
    captain = models.ForeignKey(User)
    name = models.CharField("Team Name", max_length=100, blank=False)

    def __unicode__(self):
        return self.name


class Registration(models.Model):
    user = models.ForeignKey(User)
    team = models.ForeignKey(Team, blank=True, null=True)

    nickname = models.CharField("Game Nick", max_length=100, blank=False)

    def __unicode__(self):
        return self.nickname


