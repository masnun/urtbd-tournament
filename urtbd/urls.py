from django.conf.urls import patterns, include, url
import xadmin

xadmin.autodiscover()


# Views
from django.contrib.auth.views import login, logout
from regform.views import register, home, rulebook, teams

urlpatterns = patterns(
    '',

    url(r'^$', home, name='home'),
    url(r'rulebook', rulebook, name='rulebook'),
    url(r'teams', teams, name='teams'),
    url(r'tournament/register', register),
    url(r'account/logout', logout),


    ### Includes
    url(r'^admin/', include(xadmin.site.urls)),
    url(r'', include('social_auth.urls')),
)
